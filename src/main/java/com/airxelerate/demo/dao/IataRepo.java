package com.airxelerate.demo.dao;

import com.airxelerate.demo.model.Iata;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Repository
public interface IataRepo extends JpaRepository<Iata,Integer> {

    @Transactional(readOnly = true)
    List<Iata> findByIataCode(String iataCode);

}
