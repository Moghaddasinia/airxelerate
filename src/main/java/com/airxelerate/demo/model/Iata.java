package com.airxelerate.demo.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.Date;
import javax.persistence.*;
import java.io.Serializable;

@Entity
@Data
@Builder
@NoArgsConstructor //hibernate need
@AllArgsConstructor //dto use
@Table(name = "iata")
public class Iata implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "iata_id")
    private Integer idIata;


    @Column(length=6,name = "iata_code")
    private String iataCode;



    @Column(name = "iata_date")
    private  Date iataDate;

    @Column(name = "iata_origin")
    private String iataOrigin ;

    @Column(name = "iata_destination")
    private String iataDestination ;
}
