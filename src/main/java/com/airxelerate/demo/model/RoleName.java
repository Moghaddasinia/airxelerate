package com.airxelerate.demo.model;

public enum RoleName {
    ROLE_USER,
    ROLE_ADMIN
}
