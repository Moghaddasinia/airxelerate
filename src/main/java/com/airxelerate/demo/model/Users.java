package com.airxelerate.demo.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
@Builder
@NoArgsConstructor //hibernate need
@AllArgsConstructor //dto use
@Table(name = "users")
public class Users {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private Integer userId;



    @Column(nullable = true)
    private String password;

    @Column(nullable = true)
    private String email;


      @ManyToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
      @JoinTable(name="user_role",joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
      private Set<Role> roles;


    public Users(String email, String password) {
        this.email = email;
        this.password = password;
    }
}
