package com.airxelerate.demo.manager;


import com.airxelerate.demo.dao.UserRepo;
import com.airxelerate.demo.model.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;


@Service
@Transactional
public class UserManager implements UserDetailsService {

    private final UserRepo userRepo;


    @Autowired
    public UserManager(UserRepo userRepo){

        this.userRepo=userRepo;

    }


    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Users user = null;
        try {
            user = (Users) userRepo.findByEmail(email)
                    .orElseThrow(() -> new UsernameNotFoundException("Email " + email + " not found"));
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(),
                getAuthorities(user));
    }

    private static Collection<? extends GrantedAuthority> getAuthorities(Users user) {
        String[] userRoles = user.getRoles().stream().map((role) -> role.getName()).toArray(String[]::new);
        Collection<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList(userRoles);
        return authorities;
    }


}
