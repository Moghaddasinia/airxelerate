package com.airxelerate.demo.manager;

import com.airxelerate.demo.dao.IataRepo;
import com.airxelerate.demo.model.Iata;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.crypto.Data;
import java.util.Date;
import java.util.List;

@Service
public class IataManager {

    private  final IataRepo iataRepo;
    @Autowired
    public IataManager(IataRepo iataRepo){

        this.iataRepo=iataRepo;
    }
    public List<Iata> findAll() {

        return iataRepo.findAll();
    }


    public boolean create(Iata entity) {
        iataRepo.save(entity);
        return true;
    }

    public Iata findById(Integer id) {

        return iataRepo.findById(id).get();
    }

    public void deleted(Iata iataEntity) {

        iataRepo.delete(iataEntity);
    }

    public void update(Iata edit) {

        iataRepo.save(edit);
    }

    public List<Iata> findFlightsame(String iataCode) {
        return iataRepo.findByIataCode(iataCode);
    }

}
