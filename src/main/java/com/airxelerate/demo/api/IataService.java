package com.airxelerate.demo.api;

import com.airxelerate.demo.api.dto.IataDto;
import com.airxelerate.demo.exeption.UserNotFoundExeption;
import com.airxelerate.demo.manager.IataManager;
import com.airxelerate.demo.model.Iata;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.SQLException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/iata")
public class IataService {
    Format f = new SimpleDateFormat("MM/dd/yyyy");
    public boolean duplicate=false;
    private IataManager iataManager;

    @Autowired
    public IataService(IataManager iataManager){
        this.iataManager=iataManager;
    }
    @RequestMapping("/all")
    @Produces(MediaType.APPLICATION_JSON)
    public List<IataDto> main() throws SQLException {
        List<Iata> all = iataManager.findAll();
        List<IataDto> dtos = new ArrayList<>(all.size());
        for (Iata iataEntity : all) {
            IataDto dto = new IataDto();
            dto.toDTO(iataEntity);
            dtos.add(dto);
        }
        return dtos;


    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createMain(@RequestBody IataDto dto) {

        Iata entity = new Iata();
        List<Iata> codeId =iataManager.findFlightsame(dto.getIataCode());
        if(codeId!=null) {
            for (Iata iata : codeId) {
                if (f.format(iata.getIataDate()).equals(f.format(dto.getIataDate()))) {
                    duplicate=true;
                }
            }
        }
        if(duplicate)

        {
            duplicate=false;
            return Response.accepted("Flight numbers are the same in one day!"+dto.getIataCode()).build();

        }
        else{
            dto.save(entity);
        iataManager.create(entity);
        return Response.accepted("IATA Add Successful!").build();
    }


    }


    @RequestMapping(value = "/edit", method = RequestMethod.PUT)
    @Produces(MediaType.APPLICATION_JSON)
    public Response editMain(@RequestBody IataDto dto) {
        Iata edit = new Iata();
        List<Iata> codeId = iataManager.findFlightsame(dto.getIataCode());
        if(codeId!=null) {
            for (Iata iata : codeId) {
                if (f.format(iata.getIataDate()).equals(f.format(dto.getIataDate()))) {
                    duplicate=true;
                }
            }
        }
        if(duplicate)

        {
            duplicate=false;
            return Response.accepted("Flight numbers are the same in one day!"+dto.getIataCode()).build();

        }
        else {
            dto.save(edit);
            iataManager.update(edit);
            return Response.accepted("IATA Edit Successful!").build();
        }
    }

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    @Produces(MediaType.APPLICATION_JSON)
    public IataDto read(@PathVariable Integer id) {

        try {
            Iata productEntity = iataManager.findById(id);
            return new IataDto(productEntity);
        }catch (Exception ex){
            throw new UserNotFoundExeption();
        }

    }

    @RequestMapping(value = "/dele/{id}", method = RequestMethod.DELETE)
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleted(@PathVariable Integer id) throws SQLException {
        Iata productEntity = iataManager.findById(id);
        if (productEntity == null) {
            return Response.accepted("Error").build();
        }

        iataManager.deleted(productEntity);
        return Response.accepted("IATA tiket Deleted").build();
    }
}
