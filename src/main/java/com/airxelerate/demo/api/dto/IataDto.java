package com.airxelerate.demo.api.dto;

import com.airxelerate.demo.model.Iata;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class IataDto {
    private Integer iataId;
    private String iataCode;
    private Date iataDate;
    private String iataOrigin ;
    private String iataDestination ;

    public IataDto(Iata iata)  {

        toDTO(iata);
    }



    public void toDTO(Iata iata)  {

        this.iataId = iata.getIdIata();
        this.iataCode=iata.getIataCode();
        this.iataDate=iata.getIataDate();
        this.iataOrigin=iata.getIataOrigin();
        this.iataDestination=iata.getIataDestination();



    }

    public void save(Iata iata) {
        if (iata == null) {
            return;
        }

        iata.setIdIata(this.iataId);
        iata.setIataCode(this.iataCode);
        iata.setIataDate(this.iataDate);
        iata.setIataOrigin(this.iataOrigin);
        iata.setIataDestination(this.iataDestination);

    }

}
