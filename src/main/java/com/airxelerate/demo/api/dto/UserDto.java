package com.airxelerate.demo.api.dto;



import com.airxelerate.demo.model.Role;
import com.airxelerate.demo.model.Users;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;


@Data
@NoArgsConstructor
public class UserDto {


    private Integer userId;
    private String password;
    private String email;
    private Set<Role> detailrole;

    public UserDto(Users users)  {

        toDTO(users);
    }



    public void toDTO(Users users)  {// convert obj (User) to dto (UserRegisterDTO)

        this.userId = users.getUserId();
        this.password=users.getPassword();
        this.email=users.getEmail();
        this.detailrole=users.getRoles();



    }

    public void save(Users users) {
        if (users == null) {
            return;
        }

        users.setUserId(this.userId);
        users.setPassword(this.password);
        users.setEmail(this.email);

    }


}
